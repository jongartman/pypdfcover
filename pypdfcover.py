#!/usr/bin/env python

VERSION = '0.01'
DEFAULT_FONT = 'MONACO.TTF'

from optparse import OptionParser
import os
import PIL
import pyPdf
import re
import string

class PdfCoverMaker(object):
    
    SUFFIX = '.pdf'
    
    def __init__(self, font):
        self._font = font
    
    def create(self, path, dest, title=None):
        path = os.path.abspath(path)
        dest_file = self.find_filename_from_title(title)
        dest_path = os.path.abspath(os.path.join(dest, dest_file))
        return "%s\n%s\n%s" % (title, path, dest_path)

    def find_title_guess(self, title):
        title = re.sub(r'\.(.*)$', '', title)
        t = re.sub(r'[^a-zA-Z0-9()]', ' ', title)
        t = re.sub(r'\s+?', ' ', t)
        return t.strip().title()
    
    def find_filename_from_title(self, title):
        t = re.sub(r'[^a-zA-Z0-9()]', '_', title)
        return "{0}{1}".format(t.strip('_'), self.SUFFIX)

if __name__ == "__main__":
    
    parser = OptionParser(
        usage="usage: %prog [options] directory",
        version="%prog {version}".format(version=VERSION)
    )
    parser.add_option('-a', '--automatic',
        action="store_true",
        dest="automatic_behavior",
        default=False,
        help="Automatically rename files and generate covers."
    )
    parser.add_option('--font',
        action="store",
        dest="font_file",
        default=DEFAULT_FONT,
        help="The font to use for generating cover text. Must be a TrueType font."
    )
    parser.add_option('--output',
        action="store",
        dest="output_directory",
        default='processed',
        help="The output directory for processed PDFs."
    )
    options, args = parser.parse_args()
    
    if not args:
        print "Must specify a directory to parse."
        exit(1)
    
    pdf_directory = os.path.abspath(args[0])
    output_directory = os.path.abspath(options.output_directory)
    
    if not os.path.exists(output_directory):
        print "Creating output directory: %s" % output_directory
        os.makedirs(output_directory)
    
    file_paths = os.listdir(pdf_directory)
    
    pdfMaker = PdfCoverMaker(options.font_file)
    
    processed = 0
    total = len(file_paths)
    try:
        for path in file_paths:
            processed += 1
            print "Processing %d of %d: %s" % (processed, total, path)
            
            result = None
            if options.automatic_behavior:
                result = pdfMaker.create(path, output_directory)
            else:
                title_test = pdfMaker.find_title_guess(path)
                prompt = "Best guess for title: '%s', \n<ENTER> to use, or type another title: " % title_test
                use_title = raw_input(prompt)
                
                title = None
                if not use_title:
                    # hit enter 
                    title = title_test
                else:
                    title = use_title
                
                result = pdfMaker.create(path, output_directory, title=title)
            
            if result:
                print "Done: %s" % result
    except BaseException, e:
        print e
    print ''