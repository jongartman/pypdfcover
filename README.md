pypdfcover v0.01
================

Dependencies:
-------------

- pil
- pypdf


Usage:
------

`pypdfcover ./directory`


Notes:
------

This script will help create very basic covers for PDFs. 

It works by being run against a directory, generating an image 
based on the title of the PDF and shoving that image in as the 
first page. This was done to help me see/find PDFs that I added
to iTunes much easier when searching on an iPhone or iPad.

Note: The MONACO.TTF font is included, and is freely available
to download, but I don't know if I'm allowed to distribute it.
Hopefully someone will tell me if it's not allowed.
